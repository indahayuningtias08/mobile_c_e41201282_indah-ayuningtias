import 'dart:io';

void main(){
  print("apakah yakin install?");
  stdout.write("jawab (y/t): ");
  var jawab = stdin.readLineSync();

  // menggunakan operator ternary sebagai ganti if/esle
  var hasil = (jawab == 'y') ? "anda akan menginstall aplikasi dart" : "aborted";

  print("$hasil");
}