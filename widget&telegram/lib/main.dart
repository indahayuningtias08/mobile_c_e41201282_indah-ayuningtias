import 'package:flutter/material.dart';
import 'Tugas11/Telegram.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Telegram(),
    );
  }
}

//Membuat Column
class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Column"),
        ),
        body: Column(
          children: <Widget>[
            Container(
              child: Text("Halo 1 !!!"),
              color: Colors.lime,
              padding: EdgeInsets.all(16.0),
            ),
            Container(
              child: Text("Halo 2 !!!"),
              color: Colors.purple,
              padding: EdgeInsets.all(16.0),
            ),
            Container(
              child: Text("Halo 3 !!!"),
              color: Colors.lightBlue,
              padding: EdgeInsets.all(16.0),
            ),
          ],
        ));
  }
}

//Membuat Icon
class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Icon"),
        ),
        body: Container(           
          padding: EdgeInsets.all(16.0),           
          child: Row(             
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,             
            children: <Widget>[               
              Column(                 
                children: <Widget>[                   
                  Icon(Icons.access_alarm),                   
                  Text('Alarm')                 
                  ], 
                  ),               
              Column(                 
                children: <Widget>[                   
                  Icon(Icons.phone),                   
                  Text('Phone')                 
                  ],               
                  ),               
              Column(                 
                children: <Widget>[                   
                  Icon(Icons.book),                   
                  Text('Book')                
                  ],               
                  ),             
                ],           
              ),         
        ));
  }}

  class MyForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Text Form Field"),
        ),
        body: Padding(           
          padding: const EdgeInsets.all(8.0),           
          child: Form(             
            child: Column(               
              children: <Widget>[                 
                TextFormField(                   
                  decoration: InputDecoration(hintText: "Username"),                
                   ),                 
                   TextFormField(                  
                      obscureText: true,                   
                      decoration: InputDecoration(hintText: "Password"),                 
                      ),                 
                      RaisedButton(                   
                        child: Text("Login"),                   
                        onPressed: () {},                 
                        )               
                      ],            
                    ),          
                  ), 
            ));
  }}