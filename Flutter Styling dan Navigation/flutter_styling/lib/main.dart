//Membuat flutter styling column 
import 'package:flutter/material.dart'; 
 
void main() => runApp(MyApp()); 
 
class MyApp extends StatelessWidget {   
  @override   
  Widget build(BuildContext context) {     
    return MaterialApp(       
      home: Scaffold(         
        appBar: AppBar(           
          leading: Icon(Icons.dashboard),           
          title: Text("Belajar MaterialApp Scaffold"),            
          actions: <Widget>[             
            Icon(Icons.search),            
             // Icon(Icons.find_in_page)           
          ],           
          actionsIconTheme: IconThemeData(color: Colors.yellow),           
          backgroundColor: Colors.pinkAccent,           
          bottom: PreferredSize(             
            child: Container(               
              color: Colors.orange,                
              height: 4.0,             
            ),              
            preferredSize: Size.fromHeight(4.0)           
          ),           
          centerTitle: true, 
        ),                    
        
        floatingActionButton: FloatingActionButton(           
          backgroundColor: Colors.pinkAccent,           
          child: Text('+'),           
          onPressed: () {},         
        ),         
        body: Column(children: <Widget>[   
          Container(width: 50, height: 50, decoration: BoxDecoration(color: Colors.redAccent, 
        shape: BoxShape.circle),),   
          Container(width: 50, height: 50, decoration: BoxDecoration(color: Colors.pinkAccent, 
        shape: BoxShape.circle),),   
        Container(width: 50, height: 50, decoration: BoxDecoration(color: Colors.blueAccent, 
        shape: BoxShape.circle),), 
        ],)        
               
         ),       
         debugShowCheckedModeBanner: false,     
        );   
      } 
    } 

//membuat navigator
// import 'package:flutter/material.dart'; 
// void main() {  
//   runApp(MaterialApp(    
//     home: HomePage(),  
//   )); 
// } 
// class HomePage extends StatelessWidget {  
//   @override  Widget build(BuildContext context) {    
//     return Scaffold(      
//       appBar: AppBar(        
//         title: Text('Belajar Routing'),      
//       ),      
//       body: Center(        
//         child: ElevatedButton(          
//           onPressed: () {            
//             Route route = MaterialPageRoute(builder: (context) => AboutPage());            
//             Navigator.push(context, route);          
//           },          
//           child: Text('Tap Untuk ke AboutPage'),        
//         ),     
//       ),    
//     );  
//   } 
// } 
// class AboutPage extends StatelessWidget {  
//   @override  
//   Widget build(BuildContext context) {    
//     return Scaffold(      
//       appBar: AppBar(        
//         title: Text('Tentang Aplikasi'),      
//       ),      
//       body: Center(        
//         child: ElevatedButton(          
//           onPressed: () {            
//             Navigator.pop(context);          
//           },          
//           child: Text('Kembali'),        
//         ),      
//       ),    
//     );
//   }
// }

//membuat Navigator.pushNamed
// import 'package:flutter/material.dart'; 
// import 'package:flutter_application_1/routes.dart';
// void main() {  
//   runApp(MaterialApp(    
//     onGenerateRoute: RouteGenerator.generateRoute,  
//     )); 
//   } 
 