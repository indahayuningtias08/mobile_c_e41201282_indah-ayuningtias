import 'dart:async';

void main () async{
  print('Siap. Bernyanyi.');
  line1();
  line2();
  line3();
  print(await line4());
}

Future<void> line1(){
  return Future.delayed(Duration(seconds: 2), ()=>print('Selamat Ulang Tahun.'));
}

Future<void> line2(){
  return Future.delayed(Duration(seconds: 5), ()=>print('Selamat Ulang Tahun...'));
}

Future<void> line3(){
  return Future.delayed(Duration(seconds: 8), ()=>print('Selamat Ulang Tahun.. Sahabatku.'));
}

Future<String> line4() async{
  String greeting = 'Selamat Ulang Tahun.';
  return await Future.delayed(Duration(seconds: 11), ()=>greeting);
}
